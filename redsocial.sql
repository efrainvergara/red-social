/*
 Navicat Premium Data Transfer

 Source Server         : redsocial
 Source Server Type    : MySQL
 Source Server Version : 100408
 Source Host           : localhost:3306
 Source Schema         : redsocial

 Target Server Type    : MySQL
 Target Server Version : 100408
 File Encoding         : 65001

 Date: 07/01/2020 02:12:59
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (3, '2016_06_01_000001_create_oauth_auth_codes_table', 2);
INSERT INTO `migrations` VALUES (4, '2016_06_01_000002_create_oauth_access_tokens_table', 2);
INSERT INTO `migrations` VALUES (5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 2);
INSERT INTO `migrations` VALUES (6, '2016_06_01_000004_create_oauth_clients_table', 2);
INSERT INTO `migrations` VALUES (7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 2);
INSERT INTO `migrations` VALUES (8, '2019_12_26_013618_create_products_table', 3);

-- ----------------------------
-- Table structure for oauth_access_tokens
-- ----------------------------
DROP TABLE IF EXISTS `oauth_access_tokens`;
CREATE TABLE `oauth_access_tokens`  (
  `id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NULL DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `scopes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `expires_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `oauth_access_tokens_user_id_index`(`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of oauth_access_tokens
-- ----------------------------
INSERT INTO `oauth_access_tokens` VALUES ('149e2769ef9dd44b90c5d351d7902dab9aec0d474e3943e037e66f6d79dc859cb272b0a4337a2b02', 3, 6, NULL, '[]', 0, '2019-12-26 02:00:37', '2019-12-26 02:00:37', '2020-12-26 02:00:37');
INSERT INTO `oauth_access_tokens` VALUES ('33626ddfe1e585855323ef58fda5149cc0902385083e9baf3da9b4da73ff3dc560b5a23feb8cfa2f', 3, 6, NULL, '[]', 0, '2019-12-26 01:51:29', '2019-12-26 01:51:29', '2020-12-26 01:51:29');
INSERT INTO `oauth_access_tokens` VALUES ('4b2b2880f7ba7f25489ddbdeb5fd21d49692e63ec3ee3d25631f77ea7a8b7b52d310f601ddb09a75', 3, 6, NULL, '[]', 0, '2020-01-01 01:19:56', '2020-01-01 01:19:56', '2021-01-01 01:19:56');
INSERT INTO `oauth_access_tokens` VALUES ('7a84ab2ce32778694e15c6704172d1107452d6e527fdbde479d4e56a02579a780a39cf996d951d33', 3, 6, NULL, '[]', 0, '2019-12-26 01:54:16', '2019-12-26 01:54:16', '2020-12-26 01:54:16');
INSERT INTO `oauth_access_tokens` VALUES ('c45ec6332101641af0fffc2db801dc30b0b6bac5b1007abea20d2642f6c78d8afc81556a24b8c5f6', 3, 6, NULL, '[]', 0, '2020-01-01 01:18:00', '2020-01-01 01:18:00', '2021-01-01 01:18:00');
INSERT INTO `oauth_access_tokens` VALUES ('cb1d35eb7882097c9b7a911e4b1f8c3589b133ce6ba8f1fbe6eb301db3796b3a1ffc06637e63ddd5', 3, 6, NULL, '[]', 0, '2020-01-01 01:09:51', '2020-01-01 01:09:51', '2021-01-01 01:09:51');
INSERT INTO `oauth_access_tokens` VALUES ('dbc68dbcb46416a6d2cbe0fe1591d90a6174038206d7bc505e2c636145456118466887dd1804d747', 3, 6, NULL, '[]', 0, '2019-12-26 16:45:30', '2019-12-26 16:45:30', '2020-12-26 16:45:30');

-- ----------------------------
-- Table structure for oauth_auth_codes
-- ----------------------------
DROP TABLE IF EXISTS `oauth_auth_codes`;
CREATE TABLE `oauth_auth_codes`  (
  `id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for oauth_clients
-- ----------------------------
DROP TABLE IF EXISTS `oauth_clients`;
CREATE TABLE `oauth_clients`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NULL DEFAULT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `oauth_clients_user_id_index`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of oauth_clients
-- ----------------------------
INSERT INTO `oauth_clients` VALUES (1, NULL, 'Laravel Personal Access Client', '2spraVPCKgAnH1axWKO27IqhGPXYwWp4lQpsF0Xe', 'http://localhost', 1, 0, 0, '2019-12-26 01:25:09', '2019-12-26 01:25:09');
INSERT INTO `oauth_clients` VALUES (2, NULL, 'Laravel Password Grant Client', 'F8KOJtANBfhiaaGen7UUQsWFDdyBx72623CRcdDs', 'http://localhost', 0, 1, 0, '2019-12-26 01:25:09', '2019-12-26 01:25:09');
INSERT INTO `oauth_clients` VALUES (3, NULL, 'Laravel Personal Access Client', 'Xq5LePHGIm2iUyQPt3LsKgnL83FfzWPusLeHuWAW', 'http://localhost', 1, 0, 0, '2019-12-26 01:50:08', '2019-12-26 01:50:08');
INSERT INTO `oauth_clients` VALUES (4, NULL, 'Laravel Password Grant Client', 'tyJowGC49kot5P8JiYM92vj8RSJD9qOJ8k0STNqA', 'http://localhost', 0, 1, 0, '2019-12-26 01:50:08', '2019-12-26 01:50:08');
INSERT INTO `oauth_clients` VALUES (5, NULL, 'Laravel Personal Access Client', 'iVNInaPTOLppVwdBsUYt79WwMXrgbYlqQ4LQjW88', 'http://localhost', 1, 0, 0, '2019-12-26 01:50:37', '2019-12-26 01:50:37');
INSERT INTO `oauth_clients` VALUES (6, NULL, 'Laravel Password Grant Client', 'ciLKs5oAfusOXp6alHke7jXYvRNuY6wCwesHL8L4', 'http://localhost', 0, 1, 0, '2019-12-26 01:50:37', '2019-12-26 01:50:37');

-- ----------------------------
-- Table structure for oauth_personal_access_clients
-- ----------------------------
DROP TABLE IF EXISTS `oauth_personal_access_clients`;
CREATE TABLE `oauth_personal_access_clients`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `oauth_personal_access_clients_client_id_index`(`client_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of oauth_personal_access_clients
-- ----------------------------
INSERT INTO `oauth_personal_access_clients` VALUES (1, 1, '2019-12-26 01:25:09', '2019-12-26 01:25:09');
INSERT INTO `oauth_personal_access_clients` VALUES (2, 3, '2019-12-26 01:50:08', '2019-12-26 01:50:08');
INSERT INTO `oauth_personal_access_clients` VALUES (3, 5, '2019-12-26 01:50:37', '2019-12-26 01:50:37');

-- ----------------------------
-- Table structure for oauth_refresh_tokens
-- ----------------------------
DROP TABLE IF EXISTS `oauth_refresh_tokens`;
CREATE TABLE `oauth_refresh_tokens`  (
  `id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `oauth_refresh_tokens_access_token_id_index`(`access_token_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of oauth_refresh_tokens
-- ----------------------------
INSERT INTO `oauth_refresh_tokens` VALUES ('10c19ff53237711a1097251a6cce08defddf0b19e3742777bc2c71197b443befeacae512a6ca5a41', 'dbc68dbcb46416a6d2cbe0fe1591d90a6174038206d7bc505e2c636145456118466887dd1804d747', 0, '2020-12-26 16:45:30');
INSERT INTO `oauth_refresh_tokens` VALUES ('4997f7cce147463e1995269d157821fee817306683df473f025255251666fc5df7517e50c694c16d', 'c45ec6332101641af0fffc2db801dc30b0b6bac5b1007abea20d2642f6c78d8afc81556a24b8c5f6', 0, '2021-01-01 01:18:00');
INSERT INTO `oauth_refresh_tokens` VALUES ('534584e392a4db672bb783a6ef63f66e130eebe5e444a6a3e576dc0be03352d8099b1164a4266f0b', '4b2b2880f7ba7f25489ddbdeb5fd21d49692e63ec3ee3d25631f77ea7a8b7b52d310f601ddb09a75', 0, '2021-01-01 01:19:56');
INSERT INTO `oauth_refresh_tokens` VALUES ('6d8cce5886e574e3be05ce81a676754d61ca3eb7737c6a10a1bb8801474295d220094fbe0b47484f', '149e2769ef9dd44b90c5d351d7902dab9aec0d474e3943e037e66f6d79dc859cb272b0a4337a2b02', 0, '2020-12-26 02:00:37');
INSERT INTO `oauth_refresh_tokens` VALUES ('b4429ca4639c769112d11562565604499931d29744fb7691a7c478696549728b109aeab4a0ccb4dd', '7a84ab2ce32778694e15c6704172d1107452d6e527fdbde479d4e56a02579a780a39cf996d951d33', 0, '2020-12-26 01:54:16');
INSERT INTO `oauth_refresh_tokens` VALUES ('c08039ad27f7b22441d95808f3ef1928e24a2dc699cc8c70d1ebd09c2a32a47671ae965941074d88', 'cb1d35eb7882097c9b7a911e4b1f8c3589b133ce6ba8f1fbe6eb301db3796b3a1ffc06637e63ddd5', 0, '2021-01-01 01:09:51');
INSERT INTO `oauth_refresh_tokens` VALUES ('c90573d20485c37ecb741d384783564d2be63436d99e06d6e214f6ab684c55194219ebd5a27a88ff', '33626ddfe1e585855323ef58fda5149cc0902385083e9baf3da9b4da73ff3dc560b5a23feb8cfa2f', 0, '2020-12-26 01:51:29');

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `detail` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES (1, 'hola', 'sjahdjahufisd', '2020-01-01 01:16:58', '2020-01-01 01:17:02');

-- ----------------------------
-- Table structure for tbl_calendario
-- ----------------------------
DROP TABLE IF EXISTS `tbl_calendario`;
CREATE TABLE `tbl_calendario`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_categoria` int(11) NULL DEFAULT NULL,
  `hora` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `dia` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `id_usuario` int(11) NULL DEFAULT NULL,
  `id_estado` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `updated_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id_usuario`(`id_usuario`) USING BTREE,
  INDEX `id_estado`(`id_estado`) USING BTREE,
  INDEX `id_categoria`(`id_categoria`) USING BTREE,
  CONSTRAINT `tbl_calendario_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `tbl_usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_calendario_ibfk_2` FOREIGN KEY (`id_estado`) REFERENCES `tbl_estado` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_calendario_ibfk_3` FOREIGN KEY (`id_categoria`) REFERENCES `tbl_categoria` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tbl_categoria
-- ----------------------------
DROP TABLE IF EXISTS `tbl_categoria`;
CREATE TABLE `tbl_categoria`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_color` int(11) NULL DEFAULT NULL,
  `nombre_categoria` varchar(0) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `updated_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id_color`(`id_color`) USING BTREE,
  CONSTRAINT `tbl_categoria_ibfk_1` FOREIGN KEY (`id_color`) REFERENCES `tbl_color` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tbl_color
-- ----------------------------
DROP TABLE IF EXISTS `tbl_color`;
CREATE TABLE `tbl_color`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `color_hexa` varchar(0) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `updated_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tbl_compartir
-- ----------------------------
DROP TABLE IF EXISTS `tbl_compartir`;
CREATE TABLE `tbl_compartir`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario_comparte` int(11) NULL DEFAULT NULL,
  `id_usuario_creador` int(11) NULL DEFAULT NULL,
  `id_publicacion` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `updated_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id_usuario_compartido`(`id_usuario_comparte`) USING BTREE,
  INDEX `id_usuario_creador`(`id_usuario_creador`) USING BTREE,
  INDEX `id_publicacion`(`id_publicacion`) USING BTREE,
  CONSTRAINT `tbl_compartir_ibfk_1` FOREIGN KEY (`id_usuario_comparte`) REFERENCES `tbl_usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_compartir_ibfk_2` FOREIGN KEY (`id_usuario_creador`) REFERENCES `tbl_usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_compartir_ibfk_3` FOREIGN KEY (`id_publicacion`) REFERENCES `tbl_publicacion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 47 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbl_compartir
-- ----------------------------
INSERT INTO `tbl_compartir` VALUES (24, 8, NULL, NULL, '2019-12-04 20:44:27', '2019-12-04 20:44:27');
INSERT INTO `tbl_compartir` VALUES (25, 8, NULL, NULL, '2019-12-04 20:46:08', '2019-12-04 20:46:08');
INSERT INTO `tbl_compartir` VALUES (26, 8, NULL, NULL, '2019-12-04 20:54:57', '2019-12-04 20:54:57');
INSERT INTO `tbl_compartir` VALUES (27, 8, NULL, NULL, '2019-12-04 20:55:18', '2019-12-04 20:55:18');
INSERT INTO `tbl_compartir` VALUES (28, 8, NULL, NULL, '2019-12-04 20:56:02', '2019-12-04 20:56:02');
INSERT INTO `tbl_compartir` VALUES (29, 8, NULL, NULL, '2019-12-04 20:59:36', '2019-12-04 20:59:36');
INSERT INTO `tbl_compartir` VALUES (30, 8, 8, NULL, '2019-12-04 21:04:50', '2019-12-04 21:04:50');
INSERT INTO `tbl_compartir` VALUES (31, 8, 8, NULL, '2019-12-04 21:07:06', '2019-12-04 21:07:06');
INSERT INTO `tbl_compartir` VALUES (32, 8, 8, NULL, '2019-12-04 21:07:52', '2019-12-04 21:07:52');
INSERT INTO `tbl_compartir` VALUES (33, 8, 8, NULL, '2019-12-04 21:14:51', '2019-12-04 21:14:51');
INSERT INTO `tbl_compartir` VALUES (34, 8, 8, NULL, '2019-12-04 21:17:18', '2019-12-04 21:17:18');
INSERT INTO `tbl_compartir` VALUES (35, 8, 8, NULL, '2019-12-04 21:19:56', '2019-12-04 21:19:56');
INSERT INTO `tbl_compartir` VALUES (37, 8, 8, NULL, '2019-12-04 21:29:02', '2019-12-04 21:29:02');
INSERT INTO `tbl_compartir` VALUES (41, 8, 1, 20, '2019-12-04 21:37:07', '2019-12-04 21:37:07');

-- ----------------------------
-- Table structure for tbl_correo
-- ----------------------------
DROP TABLE IF EXISTS `tbl_correo`;
CREATE TABLE `tbl_correo`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `correo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `updated_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tbl_direccion
-- ----------------------------
DROP TABLE IF EXISTS `tbl_direccion`;
CREATE TABLE `tbl_direccion`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) NULL DEFAULT NULL,
  `direccion` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `departamento` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `ciudad` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `ubicacion_lat` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `ubicacion_long` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `updated_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id_usuario`(`id_usuario`) USING BTREE,
  CONSTRAINT `tbl_direccion_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `tbl_usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbl_direccion
-- ----------------------------
INSERT INTO `tbl_direccion` VALUES (1, 11, 'dfsd123ewdw', '1', '1', NULL, NULL, '2019-12-01 23:30:04', '2019-12-01 23:30:04');
INSERT INTO `tbl_direccion` VALUES (2, 12, NULL, NULL, NULL, NULL, NULL, '2019-12-25 21:12:57', '2019-12-25 21:12:57');

-- ----------------------------
-- Table structure for tbl_estado
-- ----------------------------
DROP TABLE IF EXISTS `tbl_estado`;
CREATE TABLE `tbl_estado`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `estado` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `updated_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tbl_evento
-- ----------------------------
DROP TABLE IF EXISTS `tbl_evento`;
CREATE TABLE `tbl_evento`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `cantidad_usuarios` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `id_estado` int(11) NULL DEFAULT NULL,
  `fecha` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `hora` time(0) NULL DEFAULT NULL,
  `descripcion` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `telefono` varchar(0) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `id_galeria` int(11) NULL DEFAULT NULL,
  `id_direccion` int(11) NULL DEFAULT NULL,
  `id_correo` int(11) NULL DEFAULT NULL,
  `id_calendario` int(11) NULL DEFAULT NULL,
  `id_usuario` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id_galeria`(`id_galeria`) USING BTREE,
  INDEX `id_direccion`(`id_direccion`) USING BTREE,
  INDEX `id_correo`(`id_correo`) USING BTREE,
  INDEX `id_calendario`(`id_calendario`) USING BTREE,
  INDEX `id_usuario`(`id_usuario`) USING BTREE,
  CONSTRAINT `tbl_evento_ibfk_1` FOREIGN KEY (`id_galeria`) REFERENCES `tbl_galeria` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_evento_ibfk_2` FOREIGN KEY (`id_direccion`) REFERENCES `tbl_direccion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_evento_ibfk_3` FOREIGN KEY (`id_correo`) REFERENCES `tbl_correo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_evento_ibfk_4` FOREIGN KEY (`id_calendario`) REFERENCES `tbl_calendario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_evento_ibfk_5` FOREIGN KEY (`id_usuario`) REFERENCES `tbl_usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tbl_galeria
-- ----------------------------
DROP TABLE IF EXISTS `tbl_galeria`;
CREATE TABLE `tbl_galeria`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url_image` varchar(0) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `updated_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tbl_identificacion
-- ----------------------------
DROP TABLE IF EXISTS `tbl_identificacion`;
CREATE TABLE `tbl_identificacion`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_identificacion` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `updated_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbl_identificacion
-- ----------------------------
INSERT INTO `tbl_identificacion` VALUES (1, 'Cedula', NULL, NULL);
INSERT INTO `tbl_identificacion` VALUES (2, 'NIT', NULL, NULL);
INSERT INTO `tbl_identificacion` VALUES (3, 'Cedula de Extranjeria', NULL, NULL);

-- ----------------------------
-- Table structure for tbl_like
-- ----------------------------
DROP TABLE IF EXISTS `tbl_like`;
CREATE TABLE `tbl_like`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `like` int(11) NULL DEFAULT NULL,
  `id_publicacion` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id_publicacion`(`id_publicacion`) USING BTREE,
  CONSTRAINT `tbl_like_ibfk_1` FOREIGN KEY (`id_publicacion`) REFERENCES `tbl_publicacion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tbl_log_chat
-- ----------------------------
DROP TABLE IF EXISTS `tbl_log_chat`;
CREATE TABLE `tbl_log_chat`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario_solicitante` int(11) NULL DEFAULT NULL,
  `mensaje` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `id_usuario_solicitado` int(11) NULL DEFAULT NULL,
  `hora` time(0) NULL DEFAULT NULL,
  `conversacion` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `updated_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id_usuario_solicitante`(`id_usuario_solicitante`) USING BTREE,
  INDEX `id_usuario_solicitado`(`id_usuario_solicitado`) USING BTREE,
  CONSTRAINT `tbl_log_chat_ibfk_2` FOREIGN KEY (`id_usuario_solicitado`) REFERENCES `tbl_usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tbl_publicacion
-- ----------------------------
DROP TABLE IF EXISTS `tbl_publicacion`;
CREATE TABLE `tbl_publicacion`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) NULL DEFAULT NULL,
  `descripcion` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `fk_tipo_publicacion` int(11) NULL DEFAULT NULL,
  `url_archivo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `id_estado` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `updated_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id_estado`(`id_estado`) USING BTREE,
  INDEX `tbl_publicacion_ibfk_1`(`id_usuario`) USING BTREE,
  INDEX `fk_tipo_publicacion`(`fk_tipo_publicacion`) USING BTREE,
  CONSTRAINT `tbl_publicacion_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `tbl_usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_publicacion_ibfk_2` FOREIGN KEY (`id_estado`) REFERENCES `tbl_estado` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_publicacion_ibfk_5` FOREIGN KEY (`fk_tipo_publicacion`) REFERENCES `tbl_tipo_publicacion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 89 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbl_publicacion
-- ----------------------------
INSERT INTO `tbl_publicacion` VALUES (15, 3, 'Hola estoy muy alegre, ya casi acaba la violencia en mi pais.', 1, NULL, NULL, '2019-11-27 00:51:56', '2019-11-27 00:51:56');
INSERT INTO `tbl_publicacion` VALUES (16, 3, 'Pinche luz', 2, 'Screenshot (1).png', NULL, '2019-11-27 00:53:12', '2019-11-27 00:53:12');
INSERT INTO `tbl_publicacion` VALUES (17, 3, 'Animacion', 3, 'SampleVideo_1280x720_2mb.mp4', NULL, '2019-11-27 00:54:00', '2019-11-27 00:54:00');
INSERT INTO `tbl_publicacion` VALUES (19, 1, 'Me duelen los ojos', 1, NULL, NULL, '2019-12-03 16:02:55', '2019-12-03 16:02:55');
INSERT INTO `tbl_publicacion` VALUES (20, 1, 'Hola a todos... :)', 2, 'error subc.PNG', NULL, '2019-12-04 13:19:35', '2019-12-04 13:19:35');
INSERT INTO `tbl_publicacion` VALUES (88, 8, 'werwerwerwer', 3, 'SampleVideo_1280x720_2mb.mp4', NULL, '2020-01-07 02:11:55', '2020-01-07 02:11:55');

-- ----------------------------
-- Table structure for tbl_resena
-- ----------------------------
DROP TABLE IF EXISTS `tbl_resena`;
CREATE TABLE `tbl_resena`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) NULL DEFAULT NULL,
  `id_publicacion` int(11) NULL DEFAULT NULL,
  `comentario` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `valoracion` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `id_evento` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `updated_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id_usuario`(`id_usuario`) USING BTREE,
  INDEX `id_evento`(`id_evento`) USING BTREE,
  INDEX `id_publicacion`(`id_publicacion`) USING BTREE,
  CONSTRAINT `tbl_resena_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `tbl_usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_resena_ibfk_2` FOREIGN KEY (`id_evento`) REFERENCES `tbl_evento` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_resena_ibfk_3` FOREIGN KEY (`id_publicacion`) REFERENCES `tbl_publicacion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 47 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbl_resena
-- ----------------------------
INSERT INTO `tbl_resena` VALUES (1, 1, 20, 'cxdfgdfgdfgdfgdfg', NULL, NULL, '2019-12-04 14:33:54', '2019-12-04 14:33:54');
INSERT INTO `tbl_resena` VALUES (2, 1, 19, 'holaaaaaaaaaaaaa', NULL, NULL, '2019-12-04 14:34:26', '2019-12-04 14:34:26');
INSERT INTO `tbl_resena` VALUES (7, 8, 20, 'holaaaa', NULL, NULL, '2019-12-04 20:26:52', '2019-12-04 20:26:52');

-- ----------------------------
-- Table structure for tbl_rol
-- ----------------------------
DROP TABLE IF EXISTS `tbl_rol`;
CREATE TABLE `tbl_rol`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_rol` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `updated_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tbl_seguir
-- ----------------------------
DROP TABLE IF EXISTS `tbl_seguir`;
CREATE TABLE `tbl_seguir`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario_seguido` int(11) NULL DEFAULT NULL,
  `seguir` int(2) NULL DEFAULT NULL,
  `id_usuario_seguidor` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `updated_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id_usuario`(`id_usuario_seguido`) USING BTREE,
  INDEX `id_usuario_seguidor`(`id_usuario_seguidor`) USING BTREE,
  CONSTRAINT `tbl_seguir_ibfk_1` FOREIGN KEY (`id_usuario_seguido`) REFERENCES `tbl_usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_seguir_ibfk_2` FOREIGN KEY (`id_usuario_seguidor`) REFERENCES `tbl_usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 66 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbl_seguir
-- ----------------------------
INSERT INTO `tbl_seguir` VALUES (58, 7, NULL, 1, '2019-12-04 03:33:32', '2019-12-04 03:33:32');
INSERT INTO `tbl_seguir` VALUES (59, 11, NULL, 1, '2019-12-04 12:22:58', '2019-12-04 12:22:58');
INSERT INTO `tbl_seguir` VALUES (60, 10, NULL, 1, '2019-12-04 12:23:12', '2019-12-04 12:23:12');
INSERT INTO `tbl_seguir` VALUES (61, 8, NULL, 8, '2019-12-04 14:44:43', '2019-12-04 14:44:43');
INSERT INTO `tbl_seguir` VALUES (62, 9, NULL, 1, '2019-12-05 12:53:52', '2019-12-05 12:53:52');
INSERT INTO `tbl_seguir` VALUES (63, 4, NULL, 8, '2019-12-10 19:22:34', '2019-12-10 19:22:34');
INSERT INTO `tbl_seguir` VALUES (64, 3, NULL, 1, '2019-12-11 14:06:53', '2019-12-11 14:06:53');
INSERT INTO `tbl_seguir` VALUES (65, 2, NULL, 1, '2019-12-11 16:16:25', '2019-12-11 16:16:25');

-- ----------------------------
-- Table structure for tbl_servicios
-- ----------------------------
DROP TABLE IF EXISTS `tbl_servicios`;
CREATE TABLE `tbl_servicios`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_servicio` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `aplica_calendario` int(2) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbl_servicios
-- ----------------------------
INSERT INTO `tbl_servicios` VALUES (1, 'Servicio 1', NULL);
INSERT INTO `tbl_servicios` VALUES (2, 'Servicio 2', NULL);
INSERT INTO `tbl_servicios` VALUES (3, 'Servicio 3', NULL);

-- ----------------------------
-- Table structure for tbl_tipo_publicacion
-- ----------------------------
DROP TABLE IF EXISTS `tbl_tipo_publicacion`;
CREATE TABLE `tbl_tipo_publicacion`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_publicacion` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbl_tipo_publicacion
-- ----------------------------
INSERT INTO `tbl_tipo_publicacion` VALUES (1, 'Estado', NULL, NULL);
INSERT INTO `tbl_tipo_publicacion` VALUES (2, 'Fotos', NULL, NULL);
INSERT INTO `tbl_tipo_publicacion` VALUES (3, 'Videos', NULL, NULL);
INSERT INTO `tbl_tipo_publicacion` VALUES (4, 'Enlaces', NULL, NULL);

-- ----------------------------
-- Table structure for tbl_usuario
-- ----------------------------
DROP TABLE IF EXISTS `tbl_usuario`;
CREATE TABLE `tbl_usuario`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `apellido` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `id_identificacion` int(11) NULL DEFAULT NULL,
  `numero_identificacion` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `id_rol` int(11) NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `avatar` varchar(10000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `provider` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `provider_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `id_servicio` int(11) NULL DEFAULT NULL,
  `telefono` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `celular` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `pagina_web` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `redes_sociales` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `updated_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE,
  INDEX `id_identificacion`(`id_identificacion`) USING BTREE,
  INDEX `id_rol`(`id_rol`) USING BTREE,
  INDEX `id_servicio`(`id_servicio`) USING BTREE,
  CONSTRAINT `tbl_usuario_ibfk_1` FOREIGN KEY (`id_identificacion`) REFERENCES `tbl_identificacion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_usuario_ibfk_3` FOREIGN KEY (`id_rol`) REFERENCES `tbl_rol` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_usuario_ibfk_4` FOREIGN KEY (`id_servicio`) REFERENCES `tbl_servicios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbl_usuario
-- ----------------------------
INSERT INTO `tbl_usuario` VALUES (1, 'Efrain', 'Vergara', 'efrainvergara.udec@gmail.co', NULL, NULL, NULL, '$2y$10$C8L6e2HLUBy7erJfWlSfK.WW7dm3Z7Gsy0WlsvH5z3qM1Nzz0hNX2', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-03 16:09:40', '2019-12-03 16:09:40');
INSERT INTO `tbl_usuario` VALUES (2, 'Efrain Andres Vergara Serrato', NULL, 'efrainvergara.udec@gmail.com', NULL, NULL, NULL, NULL, 'https://graph.facebook.com/v3.3/2783724961672215/picture?type=normal', 'facebook', '2783724961672215', NULL, NULL, NULL, NULL, NULL, '2019-11-24 17:29:45', '2019-11-24 17:29:45');
INSERT INTO `tbl_usuario` VALUES (3, 'Felipe', 'Vergara', 'pipe@gmail.com', NULL, NULL, NULL, '$2y$10$2kEdIWUadH2F4YF.OjvAVeYjyBbJhcpWgKp4ZYxMi6iE4q3LzIFXG', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-03 16:09:47', '2019-12-03 16:09:47');
INSERT INTO `tbl_usuario` VALUES (4, 'prueba', 'prueba', 'prueba@gmail.com', NULL, NULL, NULL, '$2y$10$CMFBsCDj.4KEphUFsjoBBOjEnIp4s0TLshWgnSFC91IQfboHqRxNy', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-01 00:45:12', '2019-12-01 00:45:12');
INSERT INTO `tbl_usuario` VALUES (5, 'prueba2', 'prueba2', 'prueba2@gmail.com', NULL, NULL, NULL, '$2y$10$OWObxDIMTHMgPq8Y7kTAOe7x9zb3bNJ.Ff8AYETSFHELlxZsLpnzS', NULL, NULL, NULL, NULL, NULL, '54646546', NULL, NULL, '2019-12-01 00:50:13', '2019-12-01 00:50:13');
INSERT INTO `tbl_usuario` VALUES (6, 'prueba3', 'prueba3', 'prueba3@gmail.com', NULL, NULL, NULL, '$2y$10$kInDFQGYPwVOwrCZhgbO.Ox2H7dEd2Ob8fC016tO8b55JujJM/Wli', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-01 00:51:10', '2019-12-01 00:51:10');
INSERT INTO `tbl_usuario` VALUES (7, 'Pere', 'Perez', 'pepe@gmail.com', NULL, NULL, NULL, '$2y$10$LyDItUkglaIVQD6W/Ux62OZ7YgCmvEV1FVWNJipx9TzdqhdtuZiXy', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-01 23:03:48', '2019-12-01 23:03:48');
INSERT INTO `tbl_usuario` VALUES (8, 'Peppa', 'Pig', 'peppa@gmail.com', 1, '10238547458', NULL, '$2y$10$iz.7umOfHHzl3Bmt8XqLCuZaqd4DvFREjCRu27p5sbZezEusBE.zi', NULL, NULL, NULL, 2, '8963250', '3108547869', 'aresta.com.co', NULL, '2019-12-01 23:07:17', '2019-12-01 23:07:17');
INSERT INTO `tbl_usuario` VALUES (9, 'Max', 'Steel', 'max@gmail.com', 1, '1048569845', NULL, '$2y$10$UL5y6I6wzNP8bybIBBSzyO9nKxCCbcNz..vsOJFsWSh1jiJz/ZAvG', NULL, NULL, NULL, 3, '12323434', '123456890', 'asdsdfs', 'sdfsdfsdf', '2019-12-01 23:11:39', '2019-12-01 23:11:39');
INSERT INTO `tbl_usuario` VALUES (10, 'Neo', 'Anderson', 'neo@gmail.com', 1, '1050876123', NULL, '$2y$10$8LQ1p7bgiyeilg1Iy2F9geBQ7J3JRkst1IuJB.vXFEoVXnO2ySGUC', NULL, NULL, NULL, 3, '23423423', '2341342333', 'sfvcxvxcv', 'xcvxcvxcvxcv', '2019-12-01 23:24:50', '2019-12-01 23:24:50');
INSERT INTO `tbl_usuario` VALUES (11, 'hoal', 'hola', 'hola@gmail.com', 1, '1020345678', NULL, '$2y$10$b6qmUnXfDqS20lFtqVJko.5RT0.gz5satKI07VWnJR0hrEzqZEo2e', NULL, NULL, NULL, 1, '1234567', '3214567890', 'sdsfsdf', 'sdfsdfsdf', '2019-12-01 23:30:04', '2019-12-01 23:30:04');
INSERT INTO `tbl_usuario` VALUES (12, 'adsdfsdf`w', 'sddfdfgdfg', 'root@app.com', NULL, NULL, NULL, '$2y$10$ryU/NpgIX48QkuOBQUXuGu2aSIsACHkJpAf5E03TjqoLPzHZKLewm', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-25 21:12:57', '2019-12-25 21:12:57');

-- ----------------------------
-- Table structure for tbl_visto
-- ----------------------------
DROP TABLE IF EXISTS `tbl_visto`;
CREATE TABLE `tbl_visto`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `usuario_visitado` int(11) NULL DEFAULT NULL,
  `usuario_que_visita` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `updated_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `usuario_visitado`(`usuario_visitado`) USING BTREE,
  INDEX `usuario_que_visita`(`usuario_que_visita`) USING BTREE,
  CONSTRAINT `tbl_visto_ibfk_1` FOREIGN KEY (`usuario_visitado`) REFERENCES `tbl_usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nombre` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `apellido` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `avatar` varchar(10000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `provider` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `provider_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp(0) NULL DEFAULT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'Efrain Andres Vergara Serrato', NULL, 'https://graph.facebook.com/v3.3/2783724961672215/picture?type=normal', 'facebook', '2783724961672215', 'efrainvergara.udec@gmail.com', NULL, NULL, NULL, '2019-11-24 02:50:01', '2019-11-24 02:50:01');
INSERT INTO `users` VALUES (2, 'Efrain Andres Vergara Serrato', NULL, 'https://lh4.googleusercontent.com/-joC80Sge01Q/AAAAAAAAAAI/AAAAAAAAAAA/ACHi3rdUvhRm00mPuyteXZnzN8998oH9zg/photo.jpg', 'google', '114866923408787600546', 'Efrain.vergara@plannen.com', NULL, NULL, NULL, '2019-11-24 02:51:46', '2019-11-24 02:51:46');

SET FOREIGN_KEY_CHECKS = 1;
