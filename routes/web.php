<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/auth/redirect/{provider}', 'AuthController@redirect');
Route::get('/callback/{provider}', 'AuthController@callback');

Route::resource('publicaciones', 'PublicacionController');
Route::resource('perfil', 'ProfileController');

//Store Publicaciones
Route::post('storeEstado', 'PublicacionController@storeEstado')->name("publicacion.storeEstado");
Route::post('storeImages', 'PublicacionController@storeImages')->name("publicacion.storeImages");
Route::post('storevideos', 'PublicacionController@storeVideos')->name("publicacion.storeVideos");

//Store Followers
Route::resource('seguir', 'FollowController');

//Store Comentarios
Route::resource('resenas', 'ResenaController');

//Store Comentarios
Route::resource('compartir', 'CompartirController');

//Store Likes
Route::resource('like', 'LikeController');

//Chat Routes
Route::get('/messageView', 'MessageController@index');
Route::get('/messages', 'MessageController@fetch')->middleware('auth');
Route::post('/messages', 'MessageController@sentMessage')->middleware('auth');