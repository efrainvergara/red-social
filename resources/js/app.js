/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
window.moment = require('moment');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

//Status
Vue.component('status-form', require('./components/Status/StatusForm.vue').default);
Vue.component('statuses-list', require('./components/Status/Statuses-List.vue').default);
Vue.component('status', require('./components/Status/Status.vue').default);
//Status Image
Vue.component('statusImage-form', require('./components/StatusImage/StatusImageForm.vue').default);
Vue.component('statusesImage-list', require('./components/StatusImage/StatusesImage-List.vue').default);
Vue.component('statusImage', require('./components/StatusImage/StatusImage.vue').default);
//Status Video
Vue.component('statusVideo-form', require('./components/StatusVideo/StatusVideoForm.vue').default);
Vue.component('statusesVideo-list', require('./components/StatusVideo/StatusesVideo-List.vue').default);
Vue.component('statusVideo', require('./components/StatusVideo/StatusVideo.vue').default);
//Status
Vue.component('coment-form', require('./components/Coments/ComentForm.vue').default);
Vue.component('coments-list', require('./components/Coments/Coments-List.vue').default);
Vue.component('coment', require('./components/Coments/Coment.vue').default);
//Messages
Vue.component('message', require('./components/Messages/Message.vue').default);
Vue.component('message-modal', require('./components/Messages/MessageModal.vue').default);
Vue.component('sent-message', require('./components/Messages/Sent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    data: {
        messages: []
    },
    mounted(){
        this.fetchMessages();
        Echo.private('chat')
            .listen('MessageSentEvent', (e) => {
            this.messages.push({
            message: e.message.message,
            user: e.user
        })
    })
    },
    methods: {
        addMessage(message) {
            this.messages.push(message)
            axios.post('/messages', message).then(response => {
                //console.log(response)
            })
        },
        fetchMessages() {
            axios.get('/messages').then(response => {
                this.messages = response.data
        })
        }
    }

});
