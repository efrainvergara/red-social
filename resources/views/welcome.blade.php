<!DOCTYPE html>
<html lang="en">
<head>

	<title>Landing Page</title>

	<!-- Required meta tags always come first -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="x-ua-compatible" content="ie=edge">

	<!-- Main Font -->
	<script src="assets/js/libs/webfontloader.min.js"></script>

	<script>
		WebFont.load({
			google: {
				families: ['Roboto:300,400,500,700:latin']
			}
		});
	</script>

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" type="text/css" href="assets/Bootstrap/dist/css/bootstrap-reboot.css">
	<link rel="stylesheet" type="text/css" href="assets/Bootstrap/dist/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="assets/Bootstrap/dist/css/bootstrap-grid.css">

	<!-- Main Styles CSS -->
	<link rel="stylesheet" type="text/css" href="assets/css/main.css">
	<link rel="stylesheet" type="text/css" href="assets/css/fonts.min.css">



</head>

<body class="landing-page">


<!-- Preloader -->

<div id="hellopreloader">
	<div class="preloader">
		<svg width="45" height="45" stroke="#fff">
			<g fill="none" fill-rule="evenodd" stroke-width="2" transform="translate(1 1)">
				<circle cx="22" cy="22" r="6" stroke="none">
					<animate attributeName="r" begin="1.5s" calcMode="linear" dur="3s" repeatCount="indefinite" values="6;22"/>
					<animate attributeName="stroke-opacity" begin="1.5s" calcMode="linear" dur="3s" repeatCount="indefinite" values="1;0"/>
					<animate attributeName="stroke-width" begin="1.5s" calcMode="linear" dur="3s" repeatCount="indefinite" values="2;0"/>
				</circle>
				<circle cx="22" cy="22" r="6" stroke="none">
					<animate attributeName="r" begin="3s" calcMode="linear" dur="3s" repeatCount="indefinite" values="6;22"/>
					<animate attributeName="stroke-opacity" begin="3s" calcMode="linear" dur="3s" repeatCount="indefinite" values="1;0"/>
					<animate attributeName="stroke-width" begin="3s" calcMode="linear" dur="3s" repeatCount="indefinite" values="2;0"/>
				</circle>
				<circle cx="22" cy="22" r="8">
					<animate attributeName="r" begin="0s" calcMode="linear" dur="1.5s" repeatCount="indefinite" values="6;1;2;3;4;5;6"/>
				</circle>
			</g>
		</svg>

		<div class="text">Loading ...</div>
	</div>
</div>

<!-- ... end Preloader -->
<div class="content-bg-wrap"></div>


<!-- Header Standard Landing  -->

<div class="header--standard header--standard-landing" id="header--standard">
	<div class="container">
		<div class="header--standard-wrap">

			<a href="assets/#" class="logo">
				<div class="img-wrap">
					<img src="assets/img/logo.png" alt="Olympus">
					<img src="assets/img/logo-colored-small.png" alt="Olympus" class="logo-colored">
				</div>
				<div class="title-block">
					<h6 class="logo-title">olympus</h6>
					<div class="sub-title">SOCIAL NETWORK</div>
				</div>
			</a>

			<a href="assets/#" class="open-responsive-menu js-open-responsive-menu">
				<svg class="olymp-menu-icon"><use xlink:href="assets/svg-icons/sprites/icons.svg#olymp-menu-icon"></use></svg>
			</a>

			<div class="nav nav-pills nav1 header-menu">
				<div class="mCustomScrollbar">
					<ul>
						<li class="nav-item">
							<a href="assets/#" class="nav-link">Home</a>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" data-hover="dropdown" data-toggle="dropdown" href="assets/#" role="button" aria-haspopup="false" aria-expanded="false" tabindex='1'>Profile</a>
							<div class="dropdown-menu">
								<a class="dropdown-item" href="assets/#">Profile Page</a>
								<a class="dropdown-item" href="assets/#">Newsfeed</a>
								<a class="dropdown-item" href="assets/#">Post Versions</a>
							</div>
						</li>
						<li class="nav-item dropdown dropdown-has-megamenu">
							<a href="assets/#" class="nav-link dropdown-toggle" data-hover="dropdown" data-toggle="dropdown" role="button" aria-haspopup="false" aria-expanded="false" tabindex='1'>Forums</a>
							<div class="dropdown-menu megamenu">
								<div class="row">
									<div class="col col-sm-3">
										<h6 class="column-tittle">Main Links</h6>
										<a class="dropdown-item" href="assets/#">Profile Page<span class="tag-label bg-blue-light">new</span></a>
										<a class="dropdown-item" href="assets/#">Profile Page</a>
										<a class="dropdown-item" href="assets/#">Profile Page</a>
										<a class="dropdown-item" href="assets/#">Profile Page</a>
										<a class="dropdown-item" href="assets/#">Profile Page</a>
										<a class="dropdown-item" href="assets/#">Profile Page</a>
									</div>
									<div class="col col-sm-3">
										<h6 class="column-tittle">BuddyPress</h6>
										<a class="dropdown-item" href="assets/#">Profile Page</a>
										<a class="dropdown-item" href="assets/#">Profile Page</a>
										<a class="dropdown-item" href="assets/#">Profile Page<span class="tag-label bg-primary">HOT!</span></a>
										<a class="dropdown-item" href="assets/#">Profile Page</a>
										<a class="dropdown-item" href="assets/#">Profile Page</a>
										<a class="dropdown-item" href="assets/#">Profile Page</a>
									</div>
									<div class="col col-sm-3">
										<h6 class="column-tittle">Corporate</h6>
										<a class="dropdown-item" href="assets/#">Profile Page</a>
										<a class="dropdown-item" href="assets/#">Profile Page</a>
										<a class="dropdown-item" href="assets/#">Profile Page</a>
										<a class="dropdown-item" href="assets/#">Profile Page</a>
										<a class="dropdown-item" href="assets/#">Profile Page</a>
										<a class="dropdown-item" href="assets/#">Profile Page</a>
									</div>
									<div class="col col-sm-3">
										<h6 class="column-tittle">Forums</h6>
										<a class="dropdown-item" href="assets/#">Profile Page</a>
										<a class="dropdown-item" href="assets/#">Profile Page</a>
										<a class="dropdown-item" href="assets/#">Profile Page</a>
										<a class="dropdown-item" href="assets/#">Profile Page</a>
										<a class="dropdown-item" href="assets/#">Profile Page</a>
										<a class="dropdown-item" href="assets/#">Profile Page</a>
									</div>
								</div>
							</div>
						</li>
						<li class="nav-item">
							<a href="assets/#" class="nav-link">Terms & Conditions</a>
						</li>
						<li class="nav-item">
							<a href="assets/#" class="nav-link">Events</a>
						</li>
						<li class="nav-item">
							<a href="assets/#" class="nav-link">Privacy Policy</a>
						</li>
						<li class="close-responsive-menu js-close-responsive-menu">
							<svg class="olymp-close-icon"><use xlink:href="assets/svg-icons/sprites/icons.svg#olymp-close-icon"></use></svg>
						</li>
						<li class="nav-item js-expanded-menu">
							<a href="assets/#" class="nav-link">
								<svg class="olymp-menu-icon"><use xlink:href="assets/svg-icons/sprites/icons.svg#olymp-menu-icon"></use></svg>
								<svg class="olymp-close-icon"><use xlink:href="assets/svg-icons/sprites/icons.svg#olymp-close-icon"></use></svg>
							</a>
						</li>
						<li class="shoping-cart more">
							<a href="assets/#" class="nav-link">
								<svg class="olymp-shopping-bag-icon"><use xlink:href="assets/svg-icons/sprites/icons.svg#olymp-shopping-bag-icon"></use></svg>
								<span class="count-product">2</span>
							</a>
							<div class="more-dropdown shop-popup-cart">
								<ul>
									<li class="cart-product-item">
										<div class="product-thumb">
											<img src="assets/img/product1.png" alt="product">
										</div>
										<div class="product-content">
											<h6 class="title">White Enamel Mug</h6>
											<ul class="rait-stars">
												<li>
													<i class="fa fa-star star-icon c-primary" aria-hidden="true"></i>
												</li>
												<li>
													<i class="fa fa-star star-icon c-primary" aria-hidden="true"></i>
												</li>

												<li>
													<i class="fa fa-star star-icon c-primary" aria-hidden="true"></i>
												</li>
												<li>
													<i class="fa fa-star star-icon c-primary" aria-hidden="true"></i>
												</li>
												<li>
													<i class="far fa-star star-icon" aria-hidden="true"></i>
												</li>
											</ul>
											<div class="counter">x2</div>
										</div>
										<div class="product-price">$20</div>
										<div class="more">
											<svg class="olymp-little-delete"><use xlink:href="assets/svg-icons/sprites/icons.svg#olymp-little-delete"></use></svg>
										</div>
									</li>
									<li class="cart-product-item">
										<div class="product-thumb">
											<img src="assets/img/product2.png" alt="product">
										</div>
										<div class="product-content">
											<h6 class="title">Olympus Orange Shirt</h6>
											<ul class="rait-stars">
												<li>
													<i class="fa fa-star star-icon c-primary" aria-hidden="true"></i>
												</li>
												<li>
													<i class="fa fa-star star-icon c-primary" aria-hidden="true"></i>
												</li>

												<li>
													<i class="fa fa-star star-icon c-primary" aria-hidden="true"></i>
												</li>
												<li>
													<i class="fa fa-star star-icon c-primary" aria-hidden="true"></i>
												</li>
												<li>
													<i class="far fa-star star-icon" aria-hidden="true"></i>
												</li>
											</ul>
											<div class="counter">x1</div>
										</div>
										<div class="product-price">$40</div>
										<div class="more">
											<svg class="olymp-little-delete"><use xlink:href="assets/svg-icons/sprites/icons.svg#olymp-little-delete"></use></svg>
										</div>
									</li>
								</ul>

								<div class="cart-subtotal">Cart Subtotal:<span>$80</span></div>

								<div class="cart-btn-wrap">
									<a href="assets/#" class="btn btn-primary btn-sm">Go to Your Cart</a>
									<a href="assets/#" class="btn btn-purple btn-sm">Go to Checkout</a>
								</div>
							</div>
						</li>

						<li class="menu-search-item">
							<a href="assets/#" class="nav-link" data-toggle="modal" data-target="#main-popup-search">
								<svg class="olymp-magnifying-glass-icon"><use xlink:href="assets/svg-icons/sprites/icons.svg#olymp-magnifying-glass-icon"></use></svg>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- ... end Header Standard Landing  -->
<div class="header-spacer--standard"></div>

<div class="container">
	<div class="row display-flex">
		<div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
			<div class="landing-content">
				<h1>Welcome to the Biggest Social Network in the World</h1>
				<p>We are the best and biggest social network with 5 billion active users all around the world. Share you
					thoughts, write blog posts, show your favourite music via Stopify, earn badges and much more!
				</p>
				<a href="assets/#" class="btn btn-md btn-border c-white">Register Now!</a>
			</div>
		</div>

		<div class="col col-xl-5 col-lg-6 col-md-12 col-sm-12 col-12">
			
			<!-- Login-Registration Form  -->
			
			<div class="registration-login-form">
				<!-- Nav tabs -->
				<ul class="nav nav-tabs" role="tablist">
					<li class="nav-item">
						<a class="nav-link" data-toggle="tab" href="#home" role="tab">
							<svg class="olymp-login-icon"><use xlink:href="assets/svg-icons/sprites/icons.svg#olymp-login-icon"></use></svg>
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link active" data-toggle="tab" href="#profile" role="tab">
							<svg class="olymp-register-icon"><use xlink:href="assets/svg-icons/sprites/icons.svg#olymp-register-icon"></use></svg>
						</a>
					</li>
				</ul>
			
				<!-- Tab panes -->
				<div class="tab-content">
					<div class="tab-pane" id="home" role="tabpanel" data-mh="log-tab">
						<div class="title h6">Register to Olympus</div>
						<form class="content" method="POST" action="{{ route('register') }}">
                            @csrf
							<div class="row">
								<div class="col col-12 col-xl-6 col-lg-6 col-md-6 col-sm-12">
									<div class="form-group label-floating">
										<label class="control-label">Nombres</label>
                                        <input id="nombre" type="text" class="form-control @error('nombre') is-invalid @enderror" name="nombre" value="{{ old('nombre') }}" required autocomplete="nombre" autofocus>

                                        @error('nombre')
                                             <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                             </span>
                                        @enderror
       
									</div>
								</div>
								<div class="col col-12 col-xl-6 col-lg-6 col-md-6 col-sm-12">
									<div class="form-group label-floating">
										<label class="control-label">Apellidos</label>
										<input id="apellido" type="text" class="form-control @error('apellido') is-invalid @enderror" name="apellido" value="{{ old('apellido') }}" required autocomplete="apellido" autofocus>

                                    @error('apellido')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
									</div>
								</div>
								<div class="col col-12 col-xl-12 col-lg-12 col-md-12 col-sm-12">
									<div class="form-group label-floating">
										<label class="control-label">Correo electronico</label>
										<input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
									</div>

									<div class="form-group label-floating">
										<label style="display: none;" id="ltidentificacion" class="control-label">Tipo de Identificacion</label>

										<select style="display: none;" id="tidentificacion" class="form-control @error('tidentificacion') is-invalid @enderror" name="tidentificacion"  required autocomplete="tidentificacion" autofocus>
											<option value=""></option>
											<option value="1">Cedula</option>
											<option value="2">Nit</option>
											<option value="3">Cedula de extranjeria</option>
										</select>

										@error('tidentificacion')
										<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
										@enderror
										
									</div>
									<div class="form-group label-floating">
										<label style="display: none;" id="lnidentificacion"  class="control-label">Numero de Identificacion</label>

										<input style="display: none;" id="nidentificacion" type="text" class="form-control @error('nidentificacion') is-invalid @enderror" name="nidentificacion" value="{{ old('nidentificacion') }}" required autocomplete="nidentificacion" autofocus>

										@error('nidentificacion')
										<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
										@enderror
									</div>
									<div class="form-group label-floating">
										<label style="display: none;" id="lservicio" class="control-label">Servicio</label>

										<select style="display: none;" id="servicio" class="form-control @error('servicio') is-invalid @enderror" name="servicio"  required autocomplete="servicio" autofocus>
											<option value=""></option>
											<option value="1">Servicio 1</option>
											<option value="2">Servicio 2</option>
											<option value="3">Servicio 3</option>
										</select>

										@error('servicio')
										<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
										@enderror
									</div>
									<div class="form-group label-floating">
										<label style="display: none;" id="ldireccion" class="control-label">Direccion</label>

										<input style="display: none;" id="direccion" type="text" class="form-control @error('direccion') is-invalid @enderror" name="direccion" value="{{ old('direccion') }}" required autocomplete="direccion" autofocus>

										@error('direccion')
										<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
										@enderror
										
									</div>
									<div class="form-group label-floating">
										<label style="display: none;" id="ldepartamento" class="control-label">Departamento</label>

										<select style="display: none;" id="departamento" class="form-control @error('departamento') is-invalid @enderror" name="departamento"  required autocomplete="departamento" autofocus>
											<option value=""></option>
											<option value="1">Bogota d.c</option>
											<option value="2">Antioquia</option>
											<option value="3">Santander</option>
										</select>
										@error('departamento')
										<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
										@enderror

									</div>
									<div class="form-group label-floating">
										<label style="display: none;" id="lciudad" class="control-label">Ciudad</label>

										<select style="display: none;" id="ciudad" class="form-control @error('ciudad') is-invalid @enderror" name="ciudad"  required autocomplete="ciudad" autofocus>
											<option value=""></option>
											<option value="1">Bogota d.c</option>
											<option value="2">Medellin</option>
											<option value="3">Barranquilla</option>
										</select>
										@error('ciudad')
										<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
										@enderror
									</div>
									<div class="form-group label-floating">
										<label style="display: none;" id="ltelefono" class="control-label">telefono</label>

										<input style="display: none;" id="telefono" type="text" class="form-control @error('telefono') is-invalid @enderror" name="telefono" value="{{ old('telefono') }}" required autocomplete="telefono" autofocus>

										@error('telefono')
										<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
										@enderror
										
									</div>

									<div class="form-group label-floating">
										<label style="display: none;" id="lcelular" class="control-label">Celular</label>
										<input style="display: none;" id="celular" type="number" class="form-control @error('celular') is-invalid @enderror" name="celular" value="{{ old('celular') }}" required autocomplete="celular" autofocus>

										@error('celular')
										<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
										@enderror
									</div>

									<div class="form-group label-floating">
										<label style="display: none;" id="lweb" class="control-label">Pagina web</label>

										<input style="display: none;" id="web" type="text" class="form-control @error('web') is-invalid @enderror" name="web" value="{{ old('web') }}" required autocomplete="web" autofocus>

										@error('web')
										<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
										@enderror
									</div>
									<div class="form-group label-floating">
										<label style="display: none;" id="lredsocial" class="control-label">Red Social</label>

										<input style="display: none;" id="redsocial" type="text" class="form-control @error('redsocial') is-invalid @enderror" name="redsocial" value="{{ old('redsocial') }}" required autocomplete="redsocial" autofocus>

										@error('redsocial')
										<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
										@enderror
									</div>

                                    <div class="form-group label-floating">
										<label class="control-label">Contraseña</label>
										<input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
									</div>

									<div class="form-group label-floating">
										<label class="control-label">Confirmar Contraseña</label>
										<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
									</div>

									<div class="remember">
										<div class="checkbox">
											<label>
												<input id="checkServices" name="optionsCheckboxes" type="checkbox">
												Si deseas ser prestador de servicios, diligenciar los siguientes campos.
											</label>
										</div>
									</div>
			
									<div class="remember">
										<div class="checkbox">
											<label>
												<input name="optionsCheckboxes" type="checkbox">
												I accept the <a href="#">Terms and Conditions</a> of the website
											</label>
										</div>
									</div>
			
									<button type="submit" class="btn btn-purple btn-lg full-width">Complete Registration!</button>

									<div class="or"></div>
			
									<a href="{{ url('/auth/redirect/facebook') }}" class="btn btn-lg bg-facebook full-width btn-icon-left"><i class="fab fa-facebook-f" aria-hidden="true"></i>Registro con Facebook</a>
			
									<a href="{{ url('/auth/redirect/google') }}" class="btn btn-lg bg-google full-width btn-icon-left"><i class="fab fa-google" aria-hidden="true"></i>Registro con Google</a>
								</div>
							</div>
						</form>
					</div>
			
					<div class="tab-pane active" id="profile" role="tabpanel" data-mh="log-tab">
						<div class="title h6">Login to your Account</div>
						<form class="content" method="POST" action="{{ route('login') }}">
						@csrf
							<div class="row">
								<div class="col col-12 col-xl-12 col-lg-12 col-md-12 col-sm-12">
									<div class="form-group label-floating">
										<label class="control-label">Your Email</label>
                                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
									</div>
									<div class="form-group label-floating">
										<label class="control-label">Your Password</label>
										<input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
									</div>
			
									<div class="remember">
			
										<div class="checkbox">
											<label>
                                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
												Remember Me
											</label>
										</div>
										<a href="{{ route('password.request') }}" class="forgot" data-toggle="modal" data-target="#restore-password">Forgot my Password</a>
									</div>
			
									<button type="submit" class="btn btn-lg btn-primary full-width">Login</button>
			
									<div class="or"></div>
			
									<a href="{{ url('/auth/redirect/facebook') }}" class="btn btn-lg bg-facebook full-width btn-icon-left"><i class="fab fa-facebook-f" aria-hidden="true"></i>Login with Facebook</a>
			
									<a href="{{ url('/auth/redirect/google') }}" class="btn btn-lg bg-google full-width btn-icon-left"><i class="fab fa-google" aria-hidden="true"></i>Login with Google</a>
			
									<div class="or"></div> 
									<a href="#home" class="btn btn-lg full-width btn-success" data-toggle="tab" role="tab"><i></i>Registrarse</a>
									<p>Don’t you have an account? <a href="assets/#">Register Now!</a> it’s really simple and you can start enjoing all the benefits!</p>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
			
			<!-- ... end Login-Registration Form  -->		</div>
	</div>
</div>

<!-- Window-popup Restore Password -->

<div class="modal fade" id="restore-password" tabindex="-1" role="dialog" aria-labelledby="restore-password" aria-hidden="true">
	<div class="modal-dialog window-popup restore-password-popup" role="document">
		<div class="modal-content">
			<a href="assets/#" class="close icon-close" data-dismiss="modal" aria-label="Close">
				<svg class="olymp-close-icon"><use xlink:href="assets/svg-icons/sprites/icons.svg#olymp-close-icon"></use></svg>
			</a>

			<div class="modal-header">
				<h6 class="title">Restore your Password</h6>
			</div>

			<div class="modal-body">
				<form  method="get">
					<p>Enter your email and click the send code button. You’ll receive a code in your email. Please use that
						code below to change the old password for a new one.
					</p>
					<div class="form-group label-floating">
						<label class="control-label">Your Email</label>
						<input class="form-control" placeholder="" type="email" value="james-spiegel@yourmail.com">
					</div>
					<button class="btn btn-purple btn-lg full-width">Send me the Code</button>
					<div class="form-group label-floating">
						<label class="control-label">Enter the Code</label>
						<input class="form-control" placeholder="" type="text" value="">
					</div>
					<div class="form-group label-floating">
						<label class="control-label">Your New Password</label>
						<input class="form-control" placeholder="" type="password" value="olympus">
					</div>
					<button class="btn btn-primary btn-lg full-width">Change your Password!</button>
				</form>

			</div>
		</div>
	</div>
</div>

<!-- ... end Window-popup Restore Password -->


<!-- Window Popup Main Search -->

<div class="modal fade" id="main-popup-search" tabindex="-1" role="dialog" aria-labelledby="main-popup-search" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered window-popup main-popup-search" role="document">
		<div class="modal-content">
			<a href="assets/#" class="close icon-close" data-dismiss="modal" aria-label="Close">
				<svg class="olymp-close-icon"><use xlink:href="assets/svg-icons/sprites/icons.svg#olymp-close-icon"></use></svg>
			</a>
			<div class="modal-body">
				<form class="form-inline search-form" method="post">
					<div class="form-group label-floating">
						<label class="control-label">What are you looking for?</label>
						<input class="form-control bg-white" placeholder="" type="text" value="">
					</div>

					<button class="btn btn-purple btn-lg">Search</button>
				</form>
			</div>
		</div>
	</div>
</div>

<!-- ... end Window Popup Main Search -->

<!-- JS Scripts -->
<script src="assets/js/jQuery/jquery-3.4.1.js"></script>
<script src="assets/js/libs/jquery.appear.js"></script>
<script src="assets/js/libs/jquery.mousewheel.js"></script>
<script src="assets/js/libs/perfect-scrollbar.js"></script>
<script src="assets/js/libs/jquery.matchHeight.js"></script>
<script src="assets/js/libs/svgxuse.js"></script>
<script src="assets/js/libs/imagesloaded.pkgd.js"></script>
<script src="assets/js/libs/Headroom.js"></script>
<script src="assets/js/libs/velocity.js"></script>
<script src="assets/js/libs/ScrollMagic.js"></script>
<script src="assets/js/libs/jquery.waypoints.js"></script>
<script src="assets/js/libs/jquery.countTo.js"></script>
<script src="assets/js/libs/popper.min.js"></script>
<script src="assets/js/libs/material.min.js"></script>
<script src="assets/js/libs/bootstrap-select.js"></script>
<script src="assets/js/libs/smooth-scroll.js"></script>
<script src="assets/js/libs/selectize.js"></script>
<script src="assets/js/libs/swiper.jquery.js"></script>
<script src="assets/js/libs/moment.js"></script>
<script src="assets/js/libs/daterangepicker.js"></script>
<script src="assets/js/libs/fullcalendar.js"></script>
<script src="assets/js/libs/isotope.pkgd.js"></script>
<script src="assets/js/libs/ajax-pagination.js"></script>
<script src="assets/js/libs/Chart.js"></script>
<script src="assets/js/libs/chartjs-plugin-deferred.js"></script>
<script src="assets/js/libs/circle-progress.js"></script>
<script src="assets/js/libs/loader.js"></script>
<script src="assets/js/libs/run-chart.js"></script>
<script src="assets/js/libs/jquery.magnific-popup.js"></script>
<script src="assets/js/libs/jquery.gifplayer.js"></script>
<script src="assets/js/libs/mediaelement-and-player.js"></script>
<script src="assets/js/libs/mediaelement-playlist-plugin.min.js"></script>
<script src="assets/js/libs/ion.rangeSlider.js"></script>
<script src="assets/js/libs/leaflet.js"></script>
<script src="assets/js/libs/MarkerClusterGroup.js"></script>

<script src="assets/js/main.js"></script>
<script src="assets/js/libs-init/libs-init.js"></script>
<script defer src="assets/fonts/fontawesome-all.js"></script>

<script src="assets/Bootstrap/dist/js/bootstrap.bundle.js"></script>

<script>
	$(document).ready(function () {
    var ckbox = $('#checkServices');

		$('input').on('click',function () {
			if (ckbox.is(':checked')) {
				//select tipo id
				document.getElementById('tidentificacion').style.display = '';
				document.getElementById("tidentificacion").required = true;
				document.getElementById('ltidentificacion').style.display = '';
				//select numero id
				document.getElementById('nidentificacion').style.display = '';
				document.getElementById("nidentificacion").required = true;
				document.getElementById('lnidentificacion').style.display = '';
				//servicio
				document.getElementById('servicio').style.display = '';
				document.getElementById("servicio").required = true;
				document.getElementById('lservicio').style.display = '';
				//direccion
				document.getElementById('direccion').style.display = '';
				document.getElementById("direccion").required = true;
				document.getElementById('ldireccion').style.display = '';
				//select departamento
				document.getElementById('departamento').style.display = '';
				document.getElementById("departamento").required = true;
				document.getElementById('ldepartamento').style.display = '';
				//select ciudad
				document.getElementById('ciudad').style.display = '';
				document.getElementById("ciudad").required = true;
				document.getElementById('lciudad').style.display = '';
				//telefono
				document.getElementById('telefono').style.display = '';
				document.getElementById("telefono").required = true;
				document.getElementById('ltelefono').style.display = '';
				//celular
				document.getElementById('celular').style.display = '';
				document.getElementById("celular").required = true;
				document.getElementById('lcelular').style.display = '';
				//web
				document.getElementById('web').style.display = '';
				document.getElementById("web").required = true;
				document.getElementById('lweb').style.display = '';
				//servicio
				document.getElementById('redsocial').style.display = '';
				document.getElementById("redsocial").required = true;
				document.getElementById('lredsocial').style.display = '';
			} else {
				//select tipo id
				document.getElementById('tidentificacion').style.display = 'none';
				document.getElementById("tidentificacion").required = false;
				document.getElementById('ltidentificacion').style.display = 'none';
				//select numero id
				document.getElementById('nidentificacion').style.display = 'none';
				document.getElementById("nidentificacion").required = false;
				document.getElementById('lnidentificacion').style.display = 'none';
				//servicio
				document.getElementById('servicio').style.display = 'none';
				document.getElementById("servicio").required = false;
				document.getElementById('lservicio').style.display = 'none';
				//servicio
				document.getElementById('direccion').style.display = 'none';
				document.getElementById("direccion").required = false;
				document.getElementById('ldireccion').style.display = 'none';
				//servicio
				document.getElementById('departamento').style.display = 'none';
				document.getElementById("departamento").required = false;
				document.getElementById('ldepartamento').style.display = 'none';
				//servicio
				document.getElementById('ciudad').style.display = 'none';
				document.getElementById("ciudad").required = false;
				document.getElementById('lciudad').style.display = 'none';
				//servicio
				document.getElementById('telefono').style.display = 'none';
				document.getElementById("telefono").required = false;
				document.getElementById('ltelefono').style.display = 'none';
				//celular
				document.getElementById('celular').style.display = 'none';
				document.getElementById("celular").required = false;
				document.getElementById('lcelular').style.display = 'none';
				//web
				document.getElementById('web').style.display = 'none';
				document.getElementById("web").required = false;
				document.getElementById('lweb').style.display = 'none';
				//redsocial
				document.getElementById('redsocial').style.display = 'none';
				document.getElementById("redsocial").required = false;
				document.getElementById('lredsocial').style.display = 'none';
			}
		});
	});
</script>
</body>
</html>