@extends('layouts.dash')
@section('content')
{{--Inicio Mensaje Confirmar--}}
@include('alerts.success')
@include('alerts.error')
@include('alerts.errors')
{{--Fin Mensaje Confirmar--}}
<div id="app" class="container">
	<div class="row">
<!--
	<div class="application">
  <div class="video-meta">
      <img id="video-thumbnail">
    <a id="video-link"><h4 id="video-title"></h4></a>
  </div>
  <textarea id="link" rows="8"></textarea>
</div>
-->
		<!-- Main Content -->


			<statuses-list></statuses-list>


		<!-- ... end Main Content -->


		<!-- Left Sidebar -->

		<aside class="col col-xl-3 order-xl-1 col-lg-6 order-lg-2 col-md-6 col-sm-6 col-12">
			<div class="ui-block">
				
				<!-- W-Weather -->
				
				<div class="widget w-wethear">
					<a href="assets/#" class="more"><svg class="olymp-three-dots-icon"><use xlink:href="assets/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg></a>
				
					<div class="wethear-now inline-items">
						<div class="temperature-sensor">64°</div>
						<div class="max-min-temperature">
							<span>58°</span>
							<span>76°</span>
						</div>
				
						<svg class="olymp-weather-partly-sunny-icon"><use xlink:href="assets/svg-icons/sprites/icons-weather.svg#olymp-weather-partly-sunny-icon"></use></svg>
					</div>
				
					<div class="wethear-now-description">
						<div class="climate">Partly Sunny</div>
						<span>Real Feel: <span>67°</span></span>
						<span>Chance of Rain: <span>49%</span></span>
					</div>
				
				</div>
				
				<!-- W-Weather -->			</div>

			<div class="ui-block">
				
				<!-- W-Calendar -->
				
				<div class="w-calendar">
					<div class="calendar">
						<header>
							<h6 class="month">May</h6>
						</header>
						<table>
							<thead>
							<tr><td>Mon</td><td>Tue</td><td>Wed</td><td>Thu</td><td>Fri</td><td>Sat</td><td>San</td></tr>
							</thead>
							<tbody>
							<tr>
								<td data-month="12" data-day="1">1</td>
								<td data-month="12" data-day="2">
									2
								</td>
								<td data-month="12" data-day="3">3</td>
								<td data-month="12" data-day="4">4</td>
								<td data-month="12" data-day="5">5</td>
								<td data-month="12" data-day="6">6</td>
								<td data-month="12" data-day="7">7</td>
							</tr>
							<tr>
								<td data-month="12" data-day="8">8</td>
								<td data-month="12" data-day="9">9</td>
								<td data-month="12" data-day="10">10</td>
								<td data-month="12" data-day="11">11</td>
								<td data-month="12" data-day="12">12</td>
								<td data-month="12" data-day="13">13</td>
								<td data-month="12" data-day="14">14</td>
							</tr>
							<tr>
								<td data-month="12" data-day="15">15</td>
								<td data-month="12" data-day="16">16</td>
								<td data-month="12" data-day="17">17</td>
								<td data-month="12" data-day="18">18</td>
								<td data-month="12" data-day="19">19</td>
								<td data-month="12" data-day="20">20</td>
								<td data-month="12" data-day="21">21</td>
							</tr>
							<tr>
								<td data-month="12" data-day="22">22</td>
								<td data-month="12" data-day="23">23</td>
								<td data-month="12" data-day="24">24</td>
								<td data-month="12" data-day="25">25</td>
								<td data-month="12" data-day="26">26</td>
								<td data-month="12" data-day="27">27</td>
								<td data-month="12" data-day="28">28</td>
							</tr>
							<tr>
								<td data-month="12" data-day="29">29</td>
								<td data-month="12" data-day="30">30</td>
								<td data-month="12" data-day="31">31</td>
							</tr>
							</tbody>
						</table>
					</div>
				</div>
				
				
				<!-- ... end W-Calendar -->			</div>

			<div class="ui-block">
				<div class="ui-block-title">
					<h6 class="title">Pages You May Like</h6>
					<a href="assets/#" class="more"><svg class="olymp-three-dots-icon"><use xlink:href="assets/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg></a>
				</div>

				<!-- W-Friend-Pages-Added -->
				
				<ul class="widget w-friend-pages-added notification-list friend-requests">
					<li class="inline-items">
						<div class="author-thumb">
							<img src="assets/img/avatar41-sm.jpg" alt="author">
						</div>
						<div class="notification-event">
							<a href="assets/#" class="h6 notification-friend">The Marina Bar</a>
							<span class="chat-message-item">Restaurant / Bar</span>
						</div>
						<span class="notification-icon" data-toggle="tooltip" data-placement="top" data-original-title="ADD TO YOUR FAVS">
							<a href="assets/#">
								<svg class="olymp-star-icon"><use xlink:href="assets/svg-icons/sprites/icons.svg#olymp-star-icon"></use></svg>
							</a>
						</span>
				
					</li>
				
					<li class="inline-items">
						<div class="author-thumb">
							<img src="assets/img/avatar42-sm.jpg" alt="author">
						</div>
						<div class="notification-event">
							<a href="assets/#" class="h6 notification-friend">Tapronus Rock</a>
							<span class="chat-message-item">Rock Band</span>
						</div>
						<span class="notification-icon" data-toggle="tooltip" data-placement="top" data-original-title="ADD TO YOUR FAVS">
							<a href="assets/#">
								<svg class="olymp-star-icon"><use xlink:href="assets/svg-icons/sprites/icons.svg#olymp-star-icon"></use></svg>
							</a>
						</span>
				
					</li>
				
					<li class="inline-items">
						<div class="author-thumb">
							<img src="assets/img/avatar43-sm.jpg" alt="author">
						</div>
						<div class="notification-event">
							<a href="assets/#" class="h6 notification-friend">Pixel Digital Design</a>
							<span class="chat-message-item">Company</span>
						</div>
						<span class="notification-icon" data-toggle="tooltip" data-placement="top" data-original-title="ADD TO YOUR FAVS">
							<a href="assets/#">
								<svg class="olymp-star-icon"><use xlink:href="assets/svg-icons/sprites/icons.svg#olymp-star-icon"></use></svg>
							</a>
						</span>
					</li>
				
					<li class="inline-items">
						<div class="author-thumb">
							<img src="assets/img/avatar44-sm.jpg" alt="author">
						</div>
						<div class="notification-event">
							<a href="assets/#" class="h6 notification-friend">Thompson’s Custom Clothing Boutique</a>
							<span class="chat-message-item">Clothing Store</span>
						</div>
						<span class="notification-icon" data-toggle="tooltip" data-placement="top" data-original-title="ADD TO YOUR FAVS">
							<a href="assets/#">
								<svg class="olymp-star-icon"><use xlink:href="assets/svg-icons/sprites/icons.svg#olymp-star-icon"></use></svg>
							</a>
						</span>
				
					</li>
				
					<li class="inline-items">
						<div class="author-thumb">
							<img src="assets/img/avatar45-sm.jpg" alt="author">
						</div>
						<div class="notification-event">
							<a href="assets/#" class="h6 notification-friend">Crimson Agency</a>
							<span class="chat-message-item">Company</span>
						</div>
						<span class="notification-icon" data-toggle="tooltip" data-placement="top" data-original-title="ADD TO YOUR FAVS">
							<a href="assets/#">
								<svg class="olymp-star-icon"><use xlink:href="assets/svg-icons/sprites/icons.svg#olymp-star-icon"></use></svg>
							</a>
						</span>
					</li>
				
					<li class="inline-items">
						<div class="author-thumb">
							<img src="assets/img/avatar46-sm.jpg" alt="author">
						</div>
						<div class="notification-event">
							<a href="assets/#" class="h6 notification-friend">Mannequin Angel</a>
							<span class="chat-message-item">Clothing Store</span>
						</div>
						<span class="notification-icon" data-toggle="tooltip" data-placement="top" data-original-title="ADD TO YOUR FAVS">
							<a href="assets/#">
								<svg class="olymp-star-icon"><use xlink:href="assets/svg-icons/sprites/icons.svg#olymp-star-icon"></use></svg>
							</a>
						</span>
					</li>
				</ul>
				
				<!-- .. end W-Friend-Pages-Added -->
			</div>
		</aside>

		<!-- ... end Left Sidebar -->


		<!-- Right Sidebar -->

		<aside class="col col-xl-3 order-xl-3 col-lg-6 order-lg-3 col-md-6 col-sm-6 col-12">

			<div class="ui-block">
				
				<!-- W-Birthsday-Alert -->
				
				<div class="widget w-birthday-alert">
					<div class="icons-block">
						<svg class="olymp-cupcake-icon"><use xlink:href="assets/svg-icons/sprites/icons.svg#olymp-cupcake-icon"></use></svg>
						<a href="assets/#" class="more"><svg class="olymp-three-dots-icon"><use xlink:href="assets/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg></a>
					</div>
				
					<div class="content">
						<div class="author-thumb">
							<img src="assets/img/avatar48-sm.jpg" alt="author">
						</div>
						<span>Today is</span>
						<a href="assets/#" class="h4 title">Marina Valentine’s Birthday!</a>
						<p>Leave her a message with your best wishes on her profile page!</p>
					</div>
				</div>
				
				<!-- ... end W-Birthsday-Alert -->			</div>

			<div class="ui-block">
				<div class="ui-block-title">
					<h6 class="title">Friend Suggestions</h6>
					<a href="assets/#" class="more"><svg class="olymp-three-dots-icon"><use xlink:href="assets/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg></a>
				</div>

				
				
				<!-- W-Action -->
				
				<ul class="widget w-friend-pages-added notification-list friend-requests">
				@foreach($users as $user)
					<li class="inline-items">
						<div class="author-thumb">
						@if($user->avatar == "")
							<img width="36" height="36" src="assets/img/forum7.png" alt="author">
						@else
							<img width="36" height="36" src="{{$user->avatar}}" alt="author">
						@endif
						</div>
						<div class="notification-event">
							<a href="#" class="h6 notification-friend">{{$user->nombre}} {{$user->apellido}}</a>
							<span class="chat-message-item">8 Friends in Common</span>
						</div>
						<span class="notification-icon">
							<!--
							<button type="submit" class="accept-request" onclick="displayRadioValue()"> 
								<span class="icon-add without-text">
									<input type="checkbox" name="gender" value="{{$user->id}}">
										<svg class="olymp-happy-face-icon"><use xlink:href="assets/svg-icons/sprites/icons.svg#olymp-happy-face-icon"></use></svg>
								</span> 
    						</button> -->

							<label alt="Seguir" class="accept-request">
								<span class="icon-add without-text">
									<input style="display:none;" type="checkbox" name="gender" value="{{$user->id}}"  onclick="displaySeguir()"/>
									<svg class="olymp-happy-face-icon"><use xlink:href="assets/svg-icons/sprites/icons.svg#olymp-happy-face-icon"></use></svg>
								</span>
							</label>
						
						</span>
					</li>
				@endforeach
				</ul>
				
				<!-- ... end W-Action -->
			</div>

			<div class="ui-block">

				<div class="ui-block-title">
					<h6 class="title">Activity Feed</h6>
					<a href="assets/#" class="more"><svg class="olymp-three-dots-icon"><use xlink:href="assets/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg></a>
				</div>

				
				<!-- W-Activity-Feed -->
				
				<ul class="widget w-activity-feed notification-list">
					<li>
						<div class="author-thumb">
							<img src="assets/img/avatar49-sm.jpg" alt="author">
						</div>
						<div class="notification-event">
							<a href="assets/#" class="h6 notification-friend">Marina Polson</a> commented on Jason Mark’s <a href="assets/#" class="notification-link">photo.</a>.
							<span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">2 mins ago</time></span>
						</div>
					</li>
				
					<li>
						<div class="author-thumb">
							<img src="assets/img/avatar9-sm.jpg" alt="author">
						</div>
						<div class="notification-event">
							<a href="assets/#" class="h6 notification-friend">Jake Parker </a> liked Nicholas Grissom’s <a href="assets/#" class="notification-link">status update.</a>.
							<span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">5 mins ago</time></span>
						</div>
					</li>
				
					<li>
						<div class="author-thumb">
							<img src="assets/img/avatar50-sm.jpg" alt="author">
						</div>
						<div class="notification-event">
							<a href="assets/#" class="h6 notification-friend">Mary Jane Stark </a> added 20 new photos to her <a href="assets/#" class="notification-link">gallery album.</a>.
							<span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">12 mins ago</time></span>
						</div>
					</li>
				
					<li>
						<div class="author-thumb">
							<img src="assets/img/avatar51-sm.jpg" alt="author">
						</div>
						<div class="notification-event">
							<a href="assets/#" class="h6 notification-friend">Nicholas Grissom </a> updated his profile <a href="assets/#" class="notification-link">photo</a>.
							<span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">1 hour ago</time></span>
						</div>
					</li>
					<li>
						<div class="author-thumb">
							<img src="assets/img/avatar48-sm.jpg" alt="author">
						</div>
						<div class="notification-event">
							<a href="assets/#" class="h6 notification-friend">Marina Valentine </a> commented on Chris Greyson’s <a href="assets/#" class="notification-link">status update</a>.
							<span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">1 hour ago</time></span>
						</div>
					</li>
				
					<li>
						<div class="author-thumb">
							<img src="assets/img/avatar52-sm.jpg" alt="author">
						</div>
						<div class="notification-event">
							<a href="assets/#" class="h6 notification-friend">Green Goo Rock </a> posted a <a href="assets/#" class="notification-link">status update</a>.
							<span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">1 hour ago</time></span>
						</div>
					</li>
					<li>
						<div class="author-thumb">
							<img src="assets/img/avatar10-sm.jpg" alt="author">
						</div>
						<div class="notification-event">
							<a href="assets/#" class="h6 notification-friend">Elaine Dreyfuss  </a> liked your <a href="assets/#" class="notification-link">blog post</a>.
							<span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">2 hours ago</time></span>
						</div>
					</li>
				
					<li>
						<div class="author-thumb">
							<img src="assets/img/avatar10-sm.jpg" alt="author">
						</div>
						<div class="notification-event">
							<a href="assets/#" class="h6 notification-friend">Elaine Dreyfuss  </a> commented on your <a href="assets/#" class="notification-link">blog post</a>.
							<span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">2 hours ago</time></span>
						</div>
					</li>
				
					<li>
						<div class="author-thumb">
							<img src="assets/img/avatar53-sm.jpg" alt="author">
						</div>
						<div class="notification-event">
							<a href="assets/#" class="h6 notification-friend">Bruce Peterson </a> changed his <a href="assets/#" class="notification-link">profile picture</a>.
							<span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">15 hours ago</time></span>
						</div>
					</li>
				
				</ul>
				
				<!-- .. end W-Activity-Feed -->
			</div>

		</aside>

		<!-- ... end Right Sidebar -->

	</div>
</div>


<!-- Window-popup Update Header Photo -->

<div class="modal fade" id="update-header-photo" tabindex="-1" role="dialog" aria-labelledby="update-header-photo" aria-hidden="true">
	<div class="modal-dialog window-popup update-header-photo" role="document">
		<div class="modal-content">
			<a href="#" class="close icon-close" data-dismiss="modal" aria-label="Close">
				<svg class="olymp-close-icon"><use xlink:href="assets/svg-icons/sprites/icons.svg#olymp-close-icon"></use></svg>
			</a>

			<div class="modal-header">
				<h6 class="title">Update Header Photo</h6>
			</div>

			<div class="modal-body">
				<a href="#" class="upload-photo-item">
				<svg class="olymp-computer-icon"><use xlink:href="assets/svg-icons/sprites/icons.svg#olymp-computer-icon"></use></svg>

				<h6>Upload Photo</h6>
				<span>Browse your computer.</span>
			</a>

				<a href="#" class="upload-photo-item" data-toggle="modal" data-target="#choose-from-my-photo">

			<svg class="olymp-photos-icon"><use xlink:href="assets/svg-icons/sprites/icons.svg#olymp-photos-icon"></use></svg>

			<h6>Choose from My Photos</h6>
			<span>Choose from your uploaded photos</span>
		</a>
			</div>
		</div>
	</div>
</div>


<!-- ... end Window-popup Update Header Photo -->

<!-- Window-popup Choose from my Photo -->

<div class="modal fade" id="choose-from-my-photo" tabindex="-1" role="dialog" aria-labelledby="choose-from-my-photo" aria-hidden="true">
	<div class="modal-dialog window-popup choose-from-my-photo" role="document">

		<div class="modal-content">
			<a href="#" class="close icon-close" data-dismiss="modal" aria-label="Close">
				<svg class="olymp-close-icon"><use xlink:href="assets/svg-icons/sprites/icons.svg#olymp-close-icon"></use></svg>
			</a>
			<div class="modal-header">
				<h6 class="title">Choose from My Photos</h6>

				<!-- Nav tabs -->
				<ul class="nav nav-tabs" role="tablist">
					<li class="nav-item">
						<a class="nav-link active" data-toggle="tab" href="#home" role="tab" aria-expanded="true">
							<svg class="olymp-photos-icon"><use xlink:href="assets/svg-icons/sprites/icons.svg#olymp-photos-icon"></use></svg>
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" data-toggle="tab" href="#profile" role="tab" aria-expanded="false">
							<svg class="olymp-albums-icon"><use xlink:href="assets/svg-icons/sprites/icons.svg#olymp-albums-icon"></use></svg>
						</a>
					</li>
				</ul>
			</div>

			<div class="modal-body">
				<!-- Tab panes -->
				<div class="tab-content">
					<div class="tab-pane active" id="home" role="tabpanel" aria-expanded="true">

						<div class="choose-photo-item" data-mh="choose-item">
							<div class="radio">
								<label class="custom-radio">
									<img src="assets/img/choose-photo1.jpg" alt="photo">
									<input type="radio" name="optionsRadios">
								</label>
							</div>
						</div>
						<div class="choose-photo-item" data-mh="choose-item">
							<div class="radio">
								<label class="custom-radio">
									<img src="assets/img/choose-photo2.jpg" alt="photo">
									<input type="radio" name="optionsRadios">
								</label>
							</div>
						</div>
						<div class="choose-photo-item" data-mh="choose-item">
							<div class="radio">
								<label class="custom-radio">
									<img src="assets/img/choose-photo3.jpg" alt="photo">
									<input type="radio" name="optionsRadios">
								</label>
							</div>
						</div>

						<div class="choose-photo-item" data-mh="choose-item">
							<div class="radio">
								<label class="custom-radio">
									<img src="assets/img/choose-photo4.jpg" alt="photo">
									<input type="radio" name="optionsRadios">
								</label>
							</div>
						</div>
						<div class="choose-photo-item" data-mh="choose-item">
							<div class="radio">
								<label class="custom-radio">
									<img src="assets/img/choose-photo5.jpg" alt="photo">
									<input type="radio" name="optionsRadios">
								</label>
							</div>
						</div>
						<div class="choose-photo-item" data-mh="choose-item">
							<div class="radio">
								<label class="custom-radio">
									<img src="assets/img/choose-photo6.jpg" alt="photo">
									<input type="radio" name="optionsRadios">
								</label>
							</div>
						</div>

						<div class="choose-photo-item" data-mh="choose-item">
							<div class="radio">
								<label class="custom-radio">
									<img src="assets/img/choose-photo7.jpg" alt="photo">
									<input type="radio" name="optionsRadios">
								</label>
							</div>
						</div>
						<div class="choose-photo-item" data-mh="choose-item">
							<div class="radio">
								<label class="custom-radio">
									<img src="assets/img/choose-photo8.jpg" alt="photo">
									<input type="radio" name="optionsRadios">
								</label>
							</div>
						</div>
						<div class="choose-photo-item" data-mh="choose-item">
							<div class="radio">
								<label class="custom-radio">
									<img src="assets/img/choose-photo9.jpg" alt="photo">
									<input type="radio" name="optionsRadios">
								</label>
							</div>
						</div>


						<a href="assets/#" class="btn btn-secondary btn-lg btn--half-width">Cancel</a>
						<a href="assets/#" class="btn btn-primary btn-lg btn--half-width">Confirm Photo</a>

					</div>
					<div class="tab-pane" id="profile" role="tabpanel" aria-expanded="false">

						<div class="choose-photo-item" data-mh="choose-item">
							<figure>
								<img src="assets/img/choose-photo10.jpg" alt="photo">
								<figcaption>
									<a href="assets/#">South America Vacations</a>
									<span>Last Added: 2 hours ago</span>
								</figcaption>
							</figure>
						</div>
						<div class="choose-photo-item" data-mh="choose-item">
							<figure>
								<img src="assets/img/choose-photo11.jpg" alt="photo">
								<figcaption>
									<a href="assets/#">Photoshoot Summer 2016</a>
									<span>Last Added: 5 weeks ago</span>
								</figcaption>
							</figure>
						</div>
						<div class="choose-photo-item" data-mh="choose-item">
							<figure>
								<img src="assets/img/choose-photo12.jpg" alt="photo">
								<figcaption>
									<a href="assets/#">Amazing Street Food</a>
									<span>Last Added: 6 mins ago</span>
								</figcaption>
							</figure>
						</div>

						<div class="choose-photo-item" data-mh="choose-item">
							<figure>
								<img src="assets/img/choose-photo13.jpg" alt="photo">
								<figcaption>
									<a href="assets/#">Graffity & Street Art</a>
									<span>Last Added: 16 hours ago</span>
								</figcaption>
							</figure>
						</div>
						<div class="choose-photo-item" data-mh="choose-item">
							<figure>
								<img src="assets/img/choose-photo14.jpg" alt="photo">
								<figcaption>
									<a href="assets/#">Amazing Landscapes</a>
									<span>Last Added: 13 mins ago</span>
								</figcaption>
							</figure>
						</div>
						<div class="choose-photo-item" data-mh="choose-item">
							<figure>
								<img src="assets/img/choose-photo15.jpg" alt="photo">
								<figcaption>
									<a href="assets/#">The Majestic Canyon</a>
									<span>Last Added: 57 mins ago</span>
								</figcaption>
							</figure>
						</div>


						<a href="assets/#" class="btn btn-secondary btn-lg btn--half-width">Cancel</a>
						<a href="assets/#" class="btn btn-primary btn-lg disabled btn--half-width">Confirm Photo</a>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>

<!-- ... end Window-popup Choose from my Photo -->


<a class="back-to-top" href="assets/#">
	<img src="assets/svg-icons/back-to-top.svg" alt="arrow" class="back-icon">
</a>




<!-- Window-popup-CHAT for responsive min-width: 768px -->

<div class="ui-block popup-chat popup-chat-responsive" tabindex="-1" role="dialog" aria-labelledby="popup-chat-responsive" aria-hidden="true">

	<div class="modal-content">
		<div class="modal-header">
			<span class="icon-status online"></span>
			<h6 class="title" >Chat</h6>
			<div class="more">
				<svg class="olymp-three-dots-icon"><use xlink:href="assets/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg>
				<svg class="olymp-little-delete js-chat-open"><use xlink:href="assets/svg-icons/sprites/icons.svg#olymp-little-delete"></use></svg>
			</div>
		</div>
		<div class="modal-body">
			<div class="mCustomScrollbar">
				<ul class="notification-list chat-message chat-message-field">
					<li>
						<div class="author-thumb">
							<img src="assets/img/avatar14-sm.jpg" alt="author" class="mCS_img_loaded">
						</div>
						<div class="notification-event">
							<span class="chat-message-item">s</span>
							<span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">Yesterday at 8:10pm</time></span>
						</div>
					</li>

					<li>
						<div class="author-thumb">
							<img src="{{auth()->user()->avatar}}" alt="author" class="mCS_img_loaded">
						</div>
						<div class="notification-event">
							<span class="chat-message-item">Don’t worry Mathilda!</span>
							<span class="chat-message-item">I already bought everything</span>
							<span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">Yesterday at 8:29pm</time></span>
						</div>
					</li>

					<li>
						<div class="author-thumb">
							<img src="assets/img/avatar14-sm.jpg" alt="author" class="mCS_img_loaded">
						</div>
						<div class="notification-event">
							<span class="chat-message-item">ake’s gonna get the drinks</span>
							<span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">Yesterday at 8:10pm</time></span>
						</div>
					</li>
				</ul>
			</div>

			<form class="need-validation">

		<div class="form-group">
			<textarea class="form-control" placeholder="Press enter to post..."></textarea>
			<div class="add-options-message">
				<a href="assets/#" class="options-message">
					<svg class="olymp-computer-icon"><use xlink:href="assets/svg-icons/sprites/icons.svg#olymp-computer-icon"></use></svg>
				</a>
				<div class="options-message smile-block">

					<svg class="olymp-happy-sticker-icon"><use xlink:href="assets/svg-icons/sprites/icons.svg#olymp-happy-sticker-icon"></use></svg>

					<ul class="more-dropdown more-with-triangle triangle-bottom-right">
						<li>
							<a href="assets/#">
								<img src="assets/img/icon-chat1.png" alt="icon">
							</a>
						</li>
						<li>
							<a href="assets/#">
								<img src="assets/img/icon-chat2.png" alt="icon">
							</a>
						</li>
						<li>
							<a href="assets/#">
								<img src="assets/img/icon-chat3.png" alt="icon">
							</a>
						</li>
						<li>
							<a href="assets/#">
								<img src="assets/img/icon-chat4.png" alt="icon">
							</a>
						</li>
						<li>
							<a href="assets/#">
								<img src="assets/img/icon-chat5.png" alt="icon">
							</a>
						</li>
						<li>
							<a href="assets/#">
								<img src="assets/img/icon-chat6.png" alt="icon">
							</a>
						</li>
						<li>
							<a href="assets/#">
								<img src="assets/img/icon-chat7.png" alt="icon">
							</a>
						</li>
						<li>
							<a href="assets/#">
								<img src="assets/img/icon-chat8.png" alt="icon">
							</a>
						</li>
						<li>
							<a href="assets/#">
								<img src="assets/img/icon-chat9.png" alt="icon">
							</a>
						</li>
						<li>
							<a href="assets/#">
								<img src="assets/img/icon-chat10.png" alt="icon">
							</a>
						</li>
						<li>
							<a href="assets/#">
								<img src="assets/img/icon-chat11.png" alt="icon">
							</a>
						</li>
						<li>
							<a href="assets/#">
								<img src="assets/img/icon-chat12.png" alt="icon">
							</a>
						</li>
						<li>
							<a href="assets/#">
								<img src="assets/img/icon-chat13.png" alt="icon">
							</a>
						</li>
						<li>
							<a href="assets/#">
								<img src="assets/img/icon-chat14.png" alt="icon">
							</a>
						</li>
						<li>
							<a href="assets/#">
								<img src="assets/img/icon-chat15.png" alt="icon">
							</a>
						</li>
						<li>
							<a href="assets/#">
								<img src="assets/img/icon-chat16.png" alt="icon">
							</a>
						</li>
						<li>
							<a href="assets/#">
								<img src="assets/img/icon-chat17.png" alt="icon">
							</a>
						</li>
						<li>
							<a href="assets/#">
								<img src="assets/img/icon-chat18.png" alt="icon">
							</a>
						</li>
						<li>
							<a href="assets/#">
								<img src="assets/img/icon-chat19.png" alt="icon">
							</a>
						</li>
						<li>
							<a href="assets/#">
								<img src="assets/img/icon-chat20.png" alt="icon">
							</a>
						</li>
						<li>
							<a href="assets/#">
								<img src="assets/img/icon-chat21.png" alt="icon">
							</a>
						</li>
						<li>
							<a href="assets/#">
								<img src="assets/img/icon-chat22.png" alt="icon">
							</a>
						</li>
						<li>
							<a href="assets/#">
								<img src="assets/img/icon-chat23.png" alt="icon">
							</a>
						</li>
						<li>
							<a href="assets/#">
								<img src="assets/img/icon-chat24.png" alt="icon">
							</a>
						</li>
						<li>
							<a href="assets/#">
								<img src="assets/img/icon-chat25.png" alt="icon">
							</a>
						</li>
						<li>
							<a href="assets/#">
								<img src="assets/img/icon-chat26.png" alt="icon">
							</a>
						</li>
						<li>
							<a href="assets/#">
								<img src="assets/img/icon-chat27.png" alt="icon">
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>

	</form>
		</div>
	</div>
@endsection
