<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Resena extends Model
{
    use Notifiable;
    protected $table = "tbl_resena";
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_usuario','id_publicacion', 'comentario', 'valoracion'
    ];

}
