<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Publicacion;

class PublicacionController extends Controller
{
    public function index()
    {
        return Publicacion::where('id_usuario', auth()->id())->get();
      
    }

    public function storeEstado(Request $request)
    {
        $iduser = auth()->user()->id ;
        $publicacion = Publicacion::create([
            'id_usuario' => $iduser,
            'descripcion' => $request['descripcion'],
            'fk_tipo_publicacion' => 1,
        ]);
        return redirect('/home')->with('success','Publicacion creada Correctamente');
    }

    public function storeImages(Request $request)
    {
        $iduser = auth()->user()->id ;
        $input=$request->all();
        $images=array();
        if($files=$request->file('images')){
            foreach($files as $file){
                $name=$file->getClientOriginalName();
                $file->move('image',$name);
                $images[]=$name;
            }
        }
        /*Insert your data*/

        Publicacion::create( [
            'id_usuario' => $iduser,
            'url_archivo'=>  implode("|",$images),
            'descripcion' => $request['descripcion'],
            'fk_tipo_publicacion' => 2,
            //you can put other insertion here
        ]);

        return redirect('/home')->with('success','Imagen(es) Cargada(s) Correctamente');  
    }

    public function storeVideos(Request $request)
    {
        $iduser = auth()->user()->id ;
        $input=$request->all();
        $images=array();
        if($files=$request->file('images')){
            foreach($files as $file){
                $name=$file->getClientOriginalName();
                $file->move('image',$name);
                $images[]=$name;
            }
        }
        /*Insert your data*/

        Publicacion::create( [
            'id_usuario' => $iduser,
            'url_archivo'=>  implode("|",$images),
            'descripcion' => $request['descripcion'],
            'fk_tipo_publicacion' => 3,
            //you can put other insertion here
        ]);

        return redirect('/home')->with('success','Video(s) Cargado(s) Correctamente');  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $iduser = auth()->user()->id ;
        $publicacion = Publicacion::create([
            'id_usuario' => $iduser,
            'descripcion' => $request['descripcion'],
            'fk_tipo_publicacion' => 1,
        ]);
        return $publicacion;
    }

    public function update(Request $request, $id)
    {
        $publicacion = Publicacion::find($id);
        $publicacion->descripcion = $request->descripcion;
        $publicacion->save();

        return $publicacion;
    }

    public function destroy($id)
    {
        $publicacion = Publicacion::find($id);
        $publicacion->delete();
    }

}
