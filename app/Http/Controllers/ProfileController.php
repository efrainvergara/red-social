<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $iduser = auth()->user()->id ;

        $publicationsList =
        DB::select(DB::raw("SELECT
        tbl_usuario.id, 
        tbl_publicacion.descripcion, 
        tbl_publicacion.url_archivo, 
        tbl_publicacion.fk_tipo_publicacion
        FROM
        tbl_usuario
        INNER JOIN
        tbl_publicacion
        ON 
        tbl_usuario.id = tbl_publicacion.id_usuario
        INNER JOIN
        tbl_tipo_publicacion
        ON 
        tbl_publicacion.fk_tipo_publicacion = tbl_tipo_publicacion.id
        WHERE tbl_usuario.id = $iduser 
        ORDER BY tbl_publicacion.created_at DESC"));

        $publications =
        DB::select(DB::raw("SELECT
        tbl_usuario.id, 
        tbl_publicacion.descripcion,
        tbl_publicacion.fk_tipo_publicacion
        FROM
        tbl_usuario
        INNER JOIN
        tbl_publicacion
        ON 
        tbl_usuario.id = tbl_publicacion.id_usuario
        INNER JOIN
        tbl_tipo_publicacion
        ON 
        tbl_publicacion.fk_tipo_publicacion = tbl_tipo_publicacion.id
        WHERE tbl_usuario.id = $iduser AND tbl_publicacion.fk_tipo_publicacion = 1"));

        $images =
        DB::select(DB::raw("SELECT
        tbl_usuario.id, 
        tbl_publicacion.descripcion, 
        tbl_publicacion.url_archivo, 
        tbl_publicacion.fk_tipo_publicacion
        FROM
        tbl_usuario
        INNER JOIN
        tbl_publicacion
        ON 
        tbl_usuario.id = tbl_publicacion.id_usuario
        INNER JOIN
        tbl_tipo_publicacion
        ON 
        tbl_publicacion.fk_tipo_publicacion = tbl_tipo_publicacion.id
        WHERE tbl_usuario.id = $iduser AND tbl_publicacion.fk_tipo_publicacion = 2"));

        $videos =
        DB::select(DB::raw("SELECT
        tbl_usuario.id, 
        tbl_publicacion.descripcion, 
        tbl_publicacion.url_archivo, 
        tbl_publicacion.fk_tipo_publicacion
        FROM
        tbl_usuario
        INNER JOIN
        tbl_publicacion
        ON 
        tbl_usuario.id = tbl_publicacion.id_usuario
        INNER JOIN
        tbl_tipo_publicacion
        ON 
        tbl_publicacion.fk_tipo_publicacion = tbl_tipo_publicacion.id
        WHERE tbl_usuario.id = $iduser AND tbl_publicacion.fk_tipo_publicacion = 3"));

        return view('profile',compact('publicationsList' ,'publications', 'images', 'videos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
