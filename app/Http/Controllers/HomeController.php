<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use app\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $iduser = auth()->user()->id ;

        $publicationsList =
        DB::select(DB::raw("SELECT
        tbl_usuario.id as userCreador, 
        tbl_publicacion.id, 
        tbl_publicacion.descripcion, 
        tbl_publicacion.url_archivo, 
        tbl_publicacion.fk_tipo_publicacion, 
        tbl_like.id as likeid,
        tbl_like.like
        FROM
        tbl_usuario
        INNER JOIN
        tbl_publicacion
        ON 
        tbl_usuario.id = tbl_publicacion.id_usuario
        INNER JOIN
        tbl_like
        ON 
        tbl_publicacion.id = tbl_like.id_publicacion
        WHERE tbl_usuario.id = $iduser 
        ORDER BY tbl_publicacion.created_at DESC"));

        $users = User::all();

        return view('home', compact('publicationsList', 'users'));
    }
}
