<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Direccion extends Model
{
    use Notifiable;
    protected $table = "tbl_direccion";
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_usuario','direccion', 'departamento', 'ciudad', 'ubicacion_lat', 'ubicacion_long' 
    ];

}
