<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Seguir extends Model
{
    use Notifiable;
    protected $table = "tbl_seguir";
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_usuario_seguido','id_usuario_seguidor'
    ];

}
