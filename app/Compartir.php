<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Compartir extends Model
{
    use Notifiable;
    protected $table = "tbl_compartir";
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_usuario_comparte','id_usuario_creador', 'id_publicacion'
    ];

}
